<?php
	class Router
	{
		static function parse()
		{
			$routes = explode('/', $_SERVER['REQUEST_URI']);

			if(empty($routes[1]))
				$module_name = 'notes';
			else
				$module_name = $routes[1];

			$module = strtolower($module_name) . '.php';

			$controller_path = DIR_CONTROLLERS . '/' . $module;
			if (file_exists($controller_path)) {
				include_once $controller_path;
				$controller_name = $module_name . 'Controller';
				$controller = new $controller_name;
			} else
				exit("<b>error:</b> not found controller: '" . $module_name . "Controller' <br>");
				//add error 404

			$model_path = DIR_MODELS . '/' . $module;
			if (file_exists($model_path)) {
				include_once $model_path;
				$model_name = $module_name . 'Model';
				$controller->model = new $model_name;
			}
			else
				$controller->model = new Model;

			$view_path = DIR_VIEWS . '/' . $module;
			if (file_exists($view_path)) {
				include_once $view_path;
				$view_name = $module_name . 'View';
				$controller->view = new $view_name;
			}
			else
				$controller->view = new View;

			$controller->argument = $routes[2];
			$controller->index();

		}
	}