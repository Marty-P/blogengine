<?php
	class DataBase
	{
		public $db;

		function __construct()
		{
			$this->db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_HOST_PORT);

			if( $this->db->connect_errno )
				exit("<b>Error database:</b> can't connect <br>");

			$this->db->query("SET NAMES UTF8");
		}

		public function query($query){
			return $this->db->query($query);
		}

		public function getError()
		{
			return $this->db->error;
		}

		public function real_escape_string($var)
		{
			return $this->db->real_escape_string($var);
		}

		public function getNumberOfRecords($tableName)
		{
			$query = "SELECT COUNT(*) FROM {$tableName}";
			$result =  mysqli_fetch_row($this->query($query));

			if( !$this->getError() )
				return (int)$result[0];
			else
				return BLOG_ERROR_SQL_QUERY;
		}
	}