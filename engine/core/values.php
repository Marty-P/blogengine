<?php
	//error list
	define('BLOG_ERROR_SQL_QUERY',          '#90000');

	//value list
	define('BLOG_OK',                       '#10000');
	define('BLOG_PROFILE_NOT_ACTIVATED',    '#10001');
	define('BLOG_PROFILE_NOT_FOUND',        '#10002');
	define('BLOG_PROFILE_CAN_NOT_ACTIVATE', '#10003');
	define('BLOG_PROFILE_ACTIVATED',        '#10004');
	define('BLOG_POST_NOT_FOUND', 			'#10005');