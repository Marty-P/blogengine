<?php
	class View
	{
		protected $page;

		protected  $const_tpl = array(
			'{%PATH%}' => DIR_TPL
		);

		protected $var_tpl = array(
			'{%TITLE%}'         => '',
			'{%CONTENT%}'       => '',
			'{%AUTHORIZATION%}' => '',
			'{%PUBLICATION_DATA%}' => '',
			'{%AUTOR%}' => '',
			'{%TEXT%}' => '',
			'{%COMMENTS%}' => '',
			'{%TEGS%}' => ''
		);

		function __construct()
		{
			$this->page = $this->getCodePageTpl('index.html');
		}

		public function construct_page()
		{
			$flag_consts = true;
			$flag_values = true;

			// $con = 0;
			// while($flag_consts || $flag_values) {
				foreach ($this->const_tpl as $key => $value) {
					$this->page = str_replace($key, $value, $this->page);
					$flag_consts = strripos($this->page, $key);
				}

				foreach ($this->var_tpl as $key => $value) {
					$this->page = str_replace($key, $value, $this->page);
					$flag_values = strripos($this->page, $key);
				}
			// 	$con = $con +1;
			// }
			// echo $con;
		}

		public function setValue($var, $value)
		{
			$this->var_tpl[$var] = $value;
		}

		public function getCodePageTpl($name)
		{
			$file_link = DIR_TPL . '/' . $name;

			if( !file_exists($file_link) )
				exit("<b>Error display:</b> can't open " . $name . " <br>");

			$file = fopen($file_link, 'r');
			$code = fread($file, filesize($file_link));
			fclose($file);

			return $code;
		}

		public function display()
		{
			echo $this->page;
		}

		public function message($type, $text, $title='')
		{
			switch ($type) {
				case 'danger':
					$result = '<div class="row post-block block">
								<h4>Ошибка</h4>
								<div class="bg-danger msg">' . $text . '</div>
								</div>';
					return $result;
					break;

				case 'warning':
					$result = '<div class="row post-block block">
								<h4>Внимание</h4>
								<div class="bg-warning msg">' . $text . '</div>
								</div>';
					return $result;
					break;

				case 'success':
					$result = '<div class="row post-block block">
								<h4>' . $title . '</h4>
								<div class="bg-success msg">' . $text . '</div>
								</div>';
					return $result;
					break;

				case 'info':
					$result = '<div class="row post-block block">
								<h4>' . $title . '</h4>
								<div class="bg-info msg">' . $text . '</div>
								</div>';
					return $result;
					break;

				case 'primary':
					$result = '<div class="row post-block block">
								<h4>' . $title . '</h4>
								<div class="bg-primary msg">' . $text . '</div>
								</div>';
					return $result;
					break;

				default:
					$result = '<div class="row post-block block">
								<h4>' . $title . '</h4>' . $text . '</div>';
					return $result;

			}
		}


	}