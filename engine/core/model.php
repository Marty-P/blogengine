<?php
	class Model
	{
		public $db;

		function __construct()
		{
			$this->db = new DataBase();
		}

		public function getSecretKey()
		{
			$min_len = 20;

			$key = hash('sha256', mt_rand());
			$start = mt_rand(0, strlen($key)-$min_len-1);
			$end = mt_rand(0, strlen($key)-$min_len-1);

			if($start>$end)
			{
				$temp = $start;
				$start = $end;
				$end = $temp;
			}
			$len = $end-$start;
			if($len < $min_len)
				$len = $min_len;

			return substr($key, $start, $len);
		}

		public function sendEmail($to, $massage, $subject, $additional_headers = NULL, $additional_parameters = NULL)
		{
			return mail($to, $massage, $subject, $additional_headers, $additional_headers);
		}

		public function checkSession()
		{
			if(!empty(htmlspecialchars($_COOKIE["id"])) && !empty(htmlspecialchars($_COOKIE["key"]))) {
				$id = htmlspecialchars($_COOKIE["id"]);
				$key = htmlspecialchars($_COOKIE["key"]);
				$query = "SELECT id_session, locked FROM sessions WHERE id_user='{$id}' AND secret_key='{$key}'";
				$this->db->query($query);
				$result = mysqli_fetch_row($this->db->query($query));

				if($result && !$result[1])
					return true;
				else
					return false;	
			}
		}
	}