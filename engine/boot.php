<?php
    include_once DIR_CORE . '/controller.php';
    include_once DIR_CORE . '/database.php';
    include_once DIR_CORE . '/model.php';
    include_once DIR_CORE . '/view.php';
    include_once DIR_CORE . '/router.php';
    include_once DIR_CORE . '/values.php';
    Router::parse();