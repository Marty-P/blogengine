<?php
	class ConfirmationController extends Controller
	{
		public function index()
		{
			if (empty($_POST['nickname']) or empty($_POST['email']) or empty($_POST['password'])) {
				header('Location: ' . HOST . '/registration');
			}
			else {
				// if( empty($this->model->checkNick($_POST['nickname'])) ) {
				// 	if( empty(!$this->model->checkEmail($_POST['email'])) ) {
						$id = $this->model->addNewUser($_POST['nickname'], $_POST['password'], $_POST['email']);
						if ($id == BLOG_ERROR_SQL_QUERY)
							$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Регистрация не возможна, повторите попытку позже.'));
						else {
							$key = $this->model->getActivationKey($id);
							if ($key == BLOG_ERROR_SQL_QUERY)
								$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Проблемма с активацие профиля, обратитесь в техническу поддержку.'));
							else {
								$message = HOST . "/activation/" . $key;
								$this->model->sendEmail($_POST['email'], 'test', $message);
								$this->view->setValue('{%CONTENT%}', $this->view->message('success', 'Ссылка для активации аккаунта отправленна на вашу электронную почту.'));
							}
						}
				// 	}
				// 	else
				// 		$this->view->setValue('{%CONTENT%}', $this->view->message('warning', 'Пользователь с такой почтой уже зарегистрирован.'));
				// }
				// else
				// 	$this->view->setValue('{%CONTENT%}', $this->view->message('warning', 'Пользователь с таким ником уже зарегистрирован.'));

				$this->view->construct_page();
				$this->view->display();
			}
		}
	}