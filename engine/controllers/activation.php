<?php
	class ActivationController extends Controller
	{
		function index()
		{
			switch($this->model->profile_activate($this->argument))
			{
				case BLOG_PROFILE_ACTIVATED:
					$this->view->setValue('{%CONTENT%}', $this->view->message('warning','Профиль уже активирован!'));
					break;
				case BLOG_ERROR_SQL_QUERY:
					$this->view->setValue('{%CONTENT%}', $this->view->message('danger','Не возможно активировать, попробуйте позже.'));
					break;
				case BLOG_PROFILE_CAN_NOT_ACTIVATE:
					$this->view->setValue('{%CONTENT%}', $this->view->message('danger','Не возможно активировать, попробуйте позже.'));
					break;
				default:
					$this->view->setValue('{%CONTENT%}', $this->view->message('success', 'Учетная запись активирована.'));
			}

			$this->view->construct_page();
			$this->view->display();
		}
	}