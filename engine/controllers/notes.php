<?php
	class NotesController extends Controller
	{
		function index()
		{
			if($this->model->checkSession())
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('profile_side_block.html'));
			else
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('login.html'));
			
			$this->view->construct_page();
			$this->view->display();
		}
	}