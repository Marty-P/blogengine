<?php
	class LoginController extends Controller
	{
		public function index()
		{
			switch($this->model->startSession($_POST['email'], $_POST['password'])){
				case BLOG_PROFILE_NOT_ACTIVATED:
					$this->view->setValue('{%CONTENT%}', $this->view->message('warning', 'Учетная запись еще не активирована!'));
					break;
				case BLOG_PROFILE_NOT_FOUND:
					$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Логин или пароль не верны.'));
					break;
				case BLOG_ERROR_SQL_QUERY:
					$this->view->setValue('{%CONTENT%}', $this->view->message('info', 'У нас проблемы, попробуйте позже.'));
					break;
				default:
					header('Location: ' . HOST);
			}

			$this->view->construct_page();
			$this->view->display();
		}
	}