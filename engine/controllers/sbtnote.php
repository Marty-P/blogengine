<?php
	class SbtnoteController extends Controller
	{
		public function index()
		{
			if($this->model->checkSession()){
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('profile_side_block.html'));
				if(empty($_POST['text']) or empty($_POST['title']))
					$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Поля не могут быть пустыми.'));
				else{
					$id = $this->model->publish($_COOKIE["id"], $_POST['title'], $_POST['text']);
					if($id == BLOG_ERROR_SQL_QUERY)
						$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Невозможно опубликовать заметку, попробуйте позже или обратитесь в техническую поддержку.'));
					else
						header('Location: ' . HOST . '/note/' . $id);
				}
			}
			else{
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('login.html'));
				$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Для создания записи нужно авторизоваться'));
			}
			$this->view->construct_page();
			$this->view->display();
		}
	}