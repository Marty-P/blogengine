<?php
	class LogoutController extends Controller
	{
		public function index()
		{
			$this->model->stopSession();
			header('Location: ' . HOST);
		}
	}