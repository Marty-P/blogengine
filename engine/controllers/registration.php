<?php
	class RegistrationController extends Controller
	{
		public function index()
		{
			if($this->model->checkSession()){
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('profile_side_block.html'));
				$this->view->setValue('{%CONTENT%}', $this->view->message('warning','Регистрация не возможна, вы уже авторизованы.'));
			}
			else {
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('login.html'));
				$this->view->setValue('{%CONTENT%}', $this->view->getCodePageTpl('registration.html'));
			}

			$this->view->construct_page();
			$this->view->display();
		}
	}