<?php
	class EditNoteController extends Controller
	{
		public function index()
		{
			if($this->model->checkSession()){
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('profile_side_block.html'));
				$this->view->setValue('{%CONTENT%}', $this->view->getCodePageTpl('editnote.html'));
				//добавить проверку параметра
			}
			else {
				$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('login.html'));
				$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Для создания записи нужно авторизоваться'));
			}
			$this->view->construct_page();
			$this->view->display();
		}
	}