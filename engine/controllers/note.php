<?php
	class NoteController extends Controller
	{
		public function index()
		{
			if(!empty($this->argument)){
				if($this->model->checkSession())
					$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('profile_side_block.html'));
				else
					$this->view->setValue('{%AUTHORIZATION%}', $this->view->getCodePageTpl('login.html'));

				$note = $this->model->getPost($this->argument);
				if($note == BLOG_ERROR_SQL_QUERY)
					$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Запись временно недоступна.'));
				else
					if($note == BLOG_POST_NOT_FOUND)
						$this->view->setValue('{%CONTENT%}', $this->view->message('danger', 'Записи не существует.'));
					else {
						$this->view->setValue('{%CONTENT%}', $this->view->getCodePageTpl('note.html'));
						$this->view->setValue('{%AUTOR%}', $this->model->getNickAutor($note[0]));
						$this->view->setValue('{%TITLE%}', $note[1]);
						$this->view->setValue('{%TEXT%}', $note[2]);
					}
				
				$this->view->construct_page();
				$this->view->construct_page();
				$this->view->display();
			}
			else
				header('Location: ' . HOST);

		}
	}