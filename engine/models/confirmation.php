<?php
	class ConfirmationModel extends Model
	{
		public function checkNick($nick)
		{
			$query = "SELECT * FROM users WHERE nick='{$nick}'";
			return $this->db->query($query); 
		}

		public function checkEmail($email)
		{
			$query = "SELECT * FROM users WHERE email='{$email}'";
			return $this->db->query($query);
		}

		public function addNewUser($nick, $password, $email)
		{
			//добавить проверку на длинну строки
			$id = $this->db->getNumberOfRecords('users') + 1;
			$nick = $this->db->real_escape_string($nick);
			$password = hash('sha256', $password);
			$email = $this->db->real_escape_string($email);
			$query = "INSERT INTO users (`id_user`, `nick`, `password`, `email`) VALUES ('{$id}','{$nick}','{$password}','{$email}')";
			$this->db->query($query);

			if( !$this->db->getError() )
				return $id;
			else
				return BLOG_ERROR_SQL_QUERY;
		}

		public function getActivationKey($id_user)
		{
			//TODO добавить проверку на уникальность ключа
			$id = $this->db->getNumberOfRecords('activation_key_profile') + 1;

			do {
				$key = $this->getSecretKey();
				$query = "SELECT * FROM activation_key_profile WHERE secret_key='{$key}'";
				$result = NULL; //возможно не нужно
				$result = $this->db->query($query);

			}while(empty($result));

			$query = "INSERT INTO activation_key_profile (`id_activation`, `id_user`, `secret_key`) VALUES ('{$id}','{$id_user}','{$key}')";
			$this->db->query($query);
			if( !$this->db->getError() )
				return $key;
			else
				return BLOG_ERROR_SQL_QUERY;
		}



	}