<?php
	class NoteModel extends Model
	{
		public function getPost($id)
		{
			$query = "SELECT id_user, post_title, post_text, pub_date, published FROM `posts` WHERE id_post='{$id}'";
			$result = mysqli_fetch_row($this->db->query($query));

			if($this->db->getError())
				return BLOG_ERROR_SQL_QUERY;
			else
				if($result[4])
					return array($result[0], $result[1], $result[2], $result[3]);
				else
					return BLOG_POST_NOT_FOUND;
					
		}

		public function getNickAutor($id)
		{
			$query = "SELECT nick FROM `users` WHERE id_user='{$id}'";
			$result = mysqli_fetch_row($this->db->query($query));

			if($this->db->getError())
				return 'None';
			else
				return $result[0];
		}
	}