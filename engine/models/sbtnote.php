<?php
	class SbtNoteModel extends Model
	{
		public function publish($autor, $title, $text)
		{
			$id_post = $this->db->getNumberOfRecords('posts') + 1;
			$autor = $this->db->real_escape_string($autor);
			$title = $this->db->real_escape_string($title);
			$text = $this->db->real_escape_string(nl2br($text));
			$query = "INSERT INTO `posts`(`id_post`, `id_user`, `post_title`, `post_text`) VALUES ('{$id_post}', '{$autor}', '{$title}', '{$text}')";
			$this->db->query($query);

			if( !$this->db->getError() )
				return $id_post;
			else
				return BLOG_ERROR_SQL_QUERY;
		}
	}