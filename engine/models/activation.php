<?php
	class ActivationModel extends Model
	{
		public function profile_activate($key)
		{
			$key = $this->db->real_escape_string($key);
			$query = "SELECT id_user, locked FROM activation_key_profile WHERE secret_key='{$key}'";
			$result = mysqli_fetch_row($this->db->query($query));
			if($this->db->getError() != NULL)
				return BLOG_ERROR_SQL_QUERY;
			else
			{
				if(empty($result))
					return BLOG_PROFILE_CAN_NOT_ACTIVATE;
				else
					if($result[1] == 1)
						return BLOG_PROFILE_ACTIVATED;
					else{
						$query = "UPDATE activation_key_profile SET locked = TRUE WHERE secret_key='{$key}'";
						$this->db->query($query);
						$id_user = $result[0];
						$query = "UPDATE users SET active=TRUE WHERE id_user = '{$id_user}'";
						$this->db->query($query);
						return BLOG_OK;
					}
			}
		}
	}