<?php
	class LoginModel extends Model
	{
		public function startSession($email,$password)
		{
			$email = $this->db->real_escape_string($email);
			$password = hash('sha256', $password);

			$query = "SELECT id_user, active FROM users WHERE email='{$email}' AND password='{$password}'";
			$result = mysqli_fetch_row($this->db->query($query));

			if($result){
				if($result[1]){
					$key = $this->getSecretKey();
					$id_user = $result[0];
					$id_session = $this->db->getNumberOfRecords('sessions') + 1;
					$query = "INSERT INTO sessions (`id_session`, `id_user`, `secret_key`) VALUES ('{$id_session}', '{$id_user}', '{$key}')";
					$this->db->query($query);
					if($this->db->getError() != NULL)
						return BLOG_ERROR_SQL_QUERY;
					else {
						//TODO исправить куки
						setcookie('id', $id_user, time()+604800, '/');
						setcookie('key', $key, time()+604800, '/');
						return BLOG_OK;
					}
				}
				else
					return BLOG_PROFILE_NOT_ACTIVATED;

			}
			else
				return BLOG_PROFILE_NOT_FOUND;
		}
	}