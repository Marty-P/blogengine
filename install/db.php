<?php
	define('DB_HOST','127.0.0.1');
	define('DB_HOST_PORT','3306');
	define('DB_NAME','blog');
	define('DB_USER','blog');
	define('DB_PASSWORD','blog');

	$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_HOST_PORT);

	if( $db->connect_errno )
		exit("<b>Error database:</b> can't connect <br>");

	$db->query("SET NAMES UTF8");

	$query = "CREATE TABLE users(
	                            id_user INT UNSIGNED UNIQUE KEY PRIMARY KEY,
	                            nick CHAR(18) NOT NULL UNIQUE KEY,
	                            password CHAR(64) NOT NULL,
	                            email CHAR(50) NOT NULL UNIQUE KEY,
	                            reg_date TIMESTAMP NOT NULL,
	                            active BOOLEAN NOT NULL DEFAULT false
	                            )";

	$query = "CREATE TABLE activation_key_profile(
	                            id_activation INT UNSIGNED UNIQUE KEY PRIMARY KEY,
	                            id_user INT UNSIGNED NOT NULL UNIQUE KEY,
	                            secret_key CHAR(64) NOT NULL,
	                            locked BOOLEAN NOT NULL DEFAULT false
	                            )";

	$query = "CREATE TABLE sessions(
                            id_session INT UNSIGNED UNIQUE KEY PRIMARY KEY,
                            id_user INT UNSIGNED NOT NULL,
                            secret_key CHAR(64) NOT NULL,
                            locked BOOLEAN NOT NULL DEFAULT false
                            )";

	$query = "CREATE TABLE posts(
							id_post INT UNSIGNED UNIQUE KEY PRIMARY KEY,
							id_user INT UNSIGNED NOT NULL,
							post_title TINYTEXT NOT NULL,
							post_text LONGTEXT NOT NULL,
							pub_date TIMESTAMP NOT NULL,
							published BOOLEAN NOT NULL DEFAULT true,
							deleted BOOLEAN NOT NULL DEFAULT false)"
