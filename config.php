<?php
    //system const
    define('HOST', 'http://blog-engine.localhost');
    define('DIR_ENGINE', './engine');
    define('DIR_CORE', DIR_ENGINE . '/core');
    define('DIR_MODELS', DIR_ENGINE . '/models');
    define('DIR_VIEWS', DIR_ENGINE . '/views');
    define('DIR_CONTROLLERS', DIR_ENGINE . '/controllers');

    //template
    define('DIR_TPL', './templates/test');

    //cookie
    define('COOKIEDOMINE', 'blog.localhost');

    //database
    define('DB_HOST','127.0.0.1');
    define('DB_HOST_PORT','3306');
    define('DB_NAME','blog');
    define('DB_USER','blog');
    define('DB_PASSWORD','blog');